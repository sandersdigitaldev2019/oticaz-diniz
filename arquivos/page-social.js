$(document).ready(function () {
    var header = {
        'Accept': 'application/json',
        'REST-range': 'resources=0-100',
        'Content-Type': 'application/json; charset=utf-8'
    };
    
    var search = (function () {
        var functions = {
            target: function () {
                $('.apoiadores a').attr('target','_blank');
            },

            anchor: function () {
                $('html, body').animate({
                    scrollTop: $(".diniz_social .blc_2").offset().top
                }, 300);
            },

            change_year: function () {
                $('.diniz_social .time_line ul li a').on('click', function (e){
                    e.preventDefault();
                    
                    $('.diniz_social .time_line ul li a').removeClass('active');
                    $(this).addClass('active');

                    let year = $(this).text();

                    $('.diniz_social .blc_2 .content').hide();
                    $('body').removeClass('ano_2019 ano_2018 ano_2017 ano_2016');

                    $('body').addClass('ano_'+year);
                    $('.diniz_social .blc_2 .content.'+year).show();

                    functions.anchor();
                });
            },

            select: function () {
                //LISTA OS ESTADOS
                $('select.search_regiao').on('change', function () {
                    let regiao = this.value;

                    if (regiao != 'Procure por região') {
                        console.log(regiao);

                        $.ajax({
                                url: 'https://www.oticasdiniz.com.br/files/lista_participantes.js',
                                type: 'GET',
                                dataType: 'json'
                            })
                            .done(function (res) {
                                $('select.search_estado option').not('option:first').remove();
                                $('select.search_cidade option').not('option:first').remove();

                                //NEW ARRAY - ESTADOS
                                let init = new Array();
                                $(res).each(function (a, el) {
                                    if (el.REGIAO === regiao) {
                                        console.log(el);
                                        init.push(el.ESTADO);
                                    }
                                });

                                //REMOVE REPEAT ESTADOS
                                var estados = [];
                                $.each(init, function (i, el) {
                                    if ($.inArray(el, estados) === -1) {
                                        console.log(el)
                                        estados.push(el);
                                        $('select.search_estado').append('<option>' + el + '</option>');
                                    }
                                });
                            });
                    } else {
                        $('select.search_estado option').not('option:first').remove();
                    }
                });

                //LISTA AS CIDADES
                $('select.search_estado').on('change', function () {
                    let regiao = $('.search_regiao').val();
                    let estado = this.value;

                    if (estado != 'Procure por estado') {
                        $.ajax({
                                url: 'https://www.oticasdiniz.com.br/files/lista_participantes.js',
                                type: 'GET',
                                dataType: 'json'
                            })
                            .done(function (res) {
                                $('select.search_cidade option').not('option:first').remove();

                                //NEW ARRAY - CIDADES
                                let init = new Array();
                                $(res).each(function (a, el) {
                                    if (el.REGIAO === regiao && el.ESTADO === estado) {
                                        console.log(el);
                                        init.push(el.CIDADE);
                                    }
                                });

                                //REMOVE REPEAT CIDADES
                                var cidades = [];
                                $.each(init, function (i, el) {
                                    if ($.inArray(el, cidades) === -1) {
                                        console.log(el)
                                        cidades.push(el);
                                        $('select.search_cidade').append('<option>' + el + '</option>');
                                    }
                                });
                            });
                    } else {
                        $('select.search_cidade option').not('option:first').remove();
                    }
                });
            },

            btn_search: function () {
                $('.btn_search').on('click', function (e) {
                    e.preventDefault();

                    if ($('select.search_regiao').val() != 'Procure por região') {
                        var regiao = $('select.search_regiao').val();
                    } else {
                        console.log('Selecione a região');
                    }

                    if ($('select.search_estado').val() != 'Procure por estado') {
                        var estado = $('select.search_estado').val();
                    } else {
                        console.log('Selecione a estado');
                    }

                    if ($('select.search_cidade').val() != 'Procure por cidade') {
                        var cidade = $('select.search_cidade').val();
                    } else {
                        console.log('Selecione a cidade');
                    }

                    console.log('Região: ' + regiao + ' Estado: ' + estado + ' Cidade: ' + cidade);

                    $.ajax({
                            url: 'https://www.oticasdiniz.com.br/files/lista_participantes.js',
                            type: 'GET',
                            headers: header,
                            dataType: 'json',
                            crossDomain: true
                        })
                        .done(function (res) {
                            $(res).each(function (a, el) {
                                if (el.REGIAO === regiao && el.ESTADO === estado && el.CIDADE === cidade) {
                                    console.log(el);
                                    console.log(el.INSTITUICAO);
                                    $('.pop_up_list, #overlay, body').addClass('active');

                                    let content = '';
                                    content += '<li><i><svg xmlns="http://www.w3.org/2000/svg" width="26" height="37" viewBox="0 0 26 37" fill="none"><path d="M22.1211 3.79548C19.6737 1.34805 16.4194 0 12.9584 0C9.49677 0 6.24321 1.34805 3.79578 3.79548C-0.733521 8.32411 -1.29639 16.8448 2.57679 22.0073L12.9584 37L23.3246 22.0282C27.2133 16.8448 26.6504 8.32411 22.1211 3.79548ZM13.078 17.5685C10.4705 17.5685 8.34806 15.4461 8.34806 12.8385C8.34806 10.231 10.4705 8.10855 13.078 8.10855C15.6856 8.10855 17.808 10.231 17.808 12.8385C17.808 15.4461 15.6856 17.5685 13.078 17.5685Z" fill="#C4161C"/></svg></i>';
                                    content += '<p>';
                                    content += el.ENDERECO + ' - ' + el.BAIRRO + ' - ' + el.CIDADE + '/' + el.ESTADO;
                                    if (el.INSTITUICAO != '') {
                                        content += '<br><br><span> Instituicao parceira: <br>' + el.INSTITUICAO + '</span>';
                                    }
                                    content += '</p>';
                                    content += '<a href="http://maps.google.com/maps?q=' + el.ENDERECO + '+' + el.BAIRRO + '+' + el.CIDADE + '+' + el.ESTADO + '" target="_blank">Ver no mapa</a>';
                                    content += '</li>';

                                    $('.pop_up_list .content ul').append(content);
                                }
                            });
                        });
                });
            },

            close_popup: function () {
                $('.pop_up_list .head span').on('click', function () {
                    $('.pop_up_list, #overlay, body').removeClass('active');
                    $('.pop_up_list .content ul li').remove();
                });
            }
        }

        functions.target();
        functions.change_year();
        functions.select();
        functions.btn_search();
        functions.close_popup();
    })();
});