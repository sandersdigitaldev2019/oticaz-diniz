[
    {
      "CIDADE": "ABAETETUBA",
      "ESTADO": "PA",
      "ENDERECO": "AV. DOM PEDRO II, 810",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "ABREU E LIMA",
      "ESTADO": "PE",
      "ENDERECO": "AV. DUQUE DE CAXIAS,582",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ALVORADA",
      "ESTADO": "RS",
      "ENDERECO": "AV. PRESIDENTE GETULIO VARGAS,1615",
      "BAIRRO": "BELA VISTA",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ANANINDEUA",
      "ESTADO": "PA",
      "ENDERECO": "TRAVESSA WE 28 421",
      "BAIRRO": "CIDADE NOVA",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "ANANINDEUA",
      "ESTADO": "PA",
      "ENDERECO": "ROD BR 316,4500",
      "BAIRRO": "COQUEIRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "ANGRA DOS REIS",
      "ESTADO": "RJ",
      "ENDERECO": "AV. JULIO MARIA, 80",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ANGRA DOS REIS",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DA CONCEICAO, 129",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "APARECIDA DE GOIANIA",
      "ESTADO": "GO",
      "ENDERECO": "AV. DA IGUALDADE S/N",
      "BAIRRO": "SETOR GARAVELO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "APARECIDA DE GOIANIA",
      "ESTADO": "GO",
      "ENDERECO": "AV. DOM ABEL RIBEIRO S/N",
      "BAIRRO": "SETOR CENTRAL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "APUCARANA",
      "ESTADO": "PR",
      "ENDERECO": "R. PONTA GROSSA,1202",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ARACAJU",
      "ESTADO": "SE",
      "ENDERECO": "AV GONCALO PRADO ROLEMBERG, 300",
      "BAIRRO": "SALGADO FILHO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ARACATI",
      "ESTADO": "CE",
      "ENDERECO": "RUA CORONEL ALEXANDRINO,991",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ARACATUBA",
      "ESTADO": "SP",
      "ENDERECO": "PRACA RUI BARBOSA,56",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ARAPONGAS",
      "ESTADO": "PR",
      "ENDERECO": "AV. ARAPONGAS,580",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ARARAQUARA",
      "ESTADO": "SP",
      "ENDERECO": "R. NOVE DE JULHO,485",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ARAUCARIA",
      "ESTADO": "PR",
      "ENDERECO": "AV. DR VICTOR DO AMARAL, 685",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ARCOVERDE",
      "ESTADO": "PE",
      "ENDERECO": "AV. CORONEL ANTONIO JAPIASSU, 430",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ARUJA",
      "ESTADO": "SP",
      "ENDERECO": "PRACA DALILA FERREIRA BARBOSA, 36",
      "BAIRRO": "JARDIM MODELO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ASSIS",
      "ESTADO": "SP",
      "ENDERECO": "AV. RUI BARBOSA,575",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ASSU",
      "ESTADO": "RN",
      "ENDERECO": "AV. SENADOR JOAO CAMARA, 262",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ATIBAIA",
      "ESTADO": "SP",
      "ENDERECO": "RUA JOSE BIM, 200",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BAGE",
      "ESTADO": "RS",
      "ENDERECO": "AV. SETE DE SETEMBRO,713",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "BALNEARIO CAMBORIU",
      "ESTADO": "SC",
      "ENDERECO": "AV. 3ïŋ― AVENIDA,887",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "BARRETOS",
      "ESTADO": "SP",
      "ENDERECO": "VIA CONSELHEIRO ANTONIO PRADO,1400",
      "BAIRRO": "PEDRO CAVALINI",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BARRETOS",
      "ESTADO": "SP",
      "ENDERECO": "RUA 20, 685",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BATURITE",
      "ESTADO": "CE",
      "ENDERECO": "TRAV INTENDENTE BERNARDINO PROENCA,566",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "BEBEDOURO",
      "ESTADO": "SP",
      "ENDERECO": "RUA CORONEL JOAO MANOEL,336",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELEM",
      "ESTADO": "PA",
      "ENDERECO": "AV. PEDRO MIRANDA, 1414",
      "BAIRRO": "PEDREIRA",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "BELEM",
      "ESTADO": "PA",
      "ENDERECO": "TV PADRE EUTIQUIO, 1078",
      "BAIRRO": "BATISTA CAMPOS",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "BELEM",
      "ESTADO": "PA",
      "ENDERECO": "RUA SANTO ANTONIO,335",
      "BAIRRO": "CAMPINA",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUA DA BAHIA 992",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUA DOS CARIJOS,675",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―DA BAHIA,992",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. VISCONDE DE IBITURUNA,131",
      "BAIRRO": "BARREIRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. SINFRONIO BROCHADO,160",
      "BAIRRO": "BARREIRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―DOS CAETES, 410",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―DOS TAMOIOS,507",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. AFONSO PENA,387",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―DOS TUPINAMBAS,453",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―DOS TUPIS,334",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―DOS CARIJOS,675",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. ABILIO MACHADO,1929",
      "BAIRRO": "GLORIA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―URSULA PAULINO,1574",
      "BAIRRO": "BETANIA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV.ïŋ―PROFESSOR ALFREDO BALENA,157",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―PADRE PEDRO PINTO,940",
      "BAIRRO": "VENDA NOVA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "RUA PADRE PEDRO PINTO,422",
      "BAIRRO": "VENDA NOVA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. CRISTIANO MACHADO,4000",
      "BAIRRO": "UNIAO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. CRISTIANO MACHADO,11833",
      "BAIRRO": "VILA CLORIS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. ïŋ―AFONSO PENA,871",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV. CRISTOVAO COLOMBO,175",
      "BAIRRO": "SAVASSI",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV PRESIDENTE CARLOS LUZ , 3001",
      "BAIRRO": "CAICARAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO HORIZONTE",
      "ESTADO": "MG",
      "ENDERECO": "AV AFONSO PENA, 871",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BELO JARDIM",
      "ESTADO": "PE",
      "ENDERECO": "AV. DEPUTADO JOSE MENDOCA BEZERRA, 200",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "BENTO GONCALVES",
      "ESTADO": "RS",
      "ENDERECO": "R. BARAO DO RIO BRANCO,280",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "BIGUACU",
      "ESTADO": "SC",
      "ENDERECO": "AV. GETULIO VARGAS,122",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "BIRIGUI",
      "ESTADO": "SP",
      "ENDERECO": "RUA BARAO DO RIO BRANCO, 459",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BLUMENAU",
      "ESTADO": "SC",
      "ENDERECO": "RUA QUINZE DE NOVEMBRO,1357",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "BLUMENAU",
      "ESTADO": "SC",
      "ENDERECO": "VIA EXPRESSA: PAUL FRITZ KUEHNRICH,1600",
      "BAIRRO": "ITOUPAVA NORTE",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "BOM JESUS",
      "ESTADO": "PI",
      "ENDERECO": "AV. GETULIO VARGAS,653",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "BRAGANCA",
      "ESTADO": "PA",
      "ENDERECO": "TRAVESSA MARCELINO CASTANHO,153",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "BRAGANCA PAULISTA",
      "ESTADO": "SP",
      "ENDERECO": "R. CORONEL JOAO LEME, 689",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "QD 08 BL 12 LT 05",
      "BAIRRO": "SOBRADINHO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "Q CNC 02 LOTE 05",
      "BAIRRO": "TAGUATINGA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "QD. 08 BL 15 LT 07",
      "BAIRRO": "SOBRADINHO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "QD. 03",
      "BAIRRO": "PLANALTINA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "SETOR COMERCIAL SUL QUADRA 05 BLOCO C",
      "BAIRRO": "ASA SUL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "AV. PARANOA QD 18 CJ 12 8",
      "BAIRRO": "PARANOA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "QD 03 CJ J LT 41 LJ B",
      "BAIRRO": "PLANALTINA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "QNM 02 CJ E LOTE 01",
      "BAIRRO": "CEILANDIA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "Q QNM 17 CJ G LOTE 04",
      "BAIRRO": "CEILANDIA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "Q C 09 LT 11",
      "BAIRRO": "TAGUATINGA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "ENTRE QUADRAS 52/54 PROJECAO 01",
      "BAIRRO": "GAMA",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "SCS QD 07 BL A LJ 02 P2 PAV. PARTE LJ P2",
      "BAIRRO": "ASA SUL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "STN CJ J LJ T67 2 PISO ASA NORTE",
      "BAIRRO": "ASA NORTE",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "ST SCS QUADRA 08 S/N SHOPPING VENANCIO 2000",
      "BAIRRO": "ASA SUL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "QS 408 CJ G LT 08/09",
      "BAIRRO": "SAMAMBAIA NORTE",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "AV. RECANTO DAS EMAS QD 205 LOTE 04",
      "BAIRRO": "RECANTO DAS EMAS",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "RUA 48 LOTE 160 S/N",
      "BAIRRO": "CENTRO SAO SEBASTIAO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "ST SDN S/N",
      "BAIRRO": "ASA NORTE",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "Q QNM 34 AREA ESPECIAL 1, S/N",
      "BAIRRO": "TAGUATINGA NORTE (TAGUATINGA)",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "SHIS QI 5 BLOCO E 25",
      "BAIRRO": "SET HAB INDIVIDUAIS SUL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "Q CENTRAL BL 5 LJ 05",
      "BAIRRO": "SOBRADINHO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BRASILIA",
      "ESTADO": "DF",
      "ENDERECO": "SHIS QI 5 BLOCO E 25",
      "BAIRRO": "SET HAB INDIVIDUAIS SUL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "BREVES",
      "ESTADO": "PA",
      "ENDERECO": "AV. RIO BRANCO,325",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "BRUSQUE",
      "ESTADO": "SC",
      "ENDERECO": "AV. CONSUL CARLOS RENAUX,107",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CABO DE SANTO AGOSTINHO",
      "ESTADO": "PE",
      "ENDERECO": "AV. HISTORIADOR PEREIRA DA COSTA,287",
      "BAIRRO": "SAO JUDAS TADEU",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CABREUVA",
      "ESTADO": "SP",
      "ENDERECO": "RUA MARANHAO, 316",
      "BAIRRO": "JACARE",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CACAPAVA",
      "ESTADO": "SP",
      "ENDERECO": "RUA CAPITAO JOAO RAMOS, 38",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CACAPAVA",
      "ESTADO": "SP",
      "ENDERECO": "RUA MARQUES DE HERVAL, 333",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CACHOEIRA DO SUL",
      "ESTADO": "RS",
      "ENDERECO": "RUA SETE DE SETEMBRO,1474",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CACHOEIRO DE ITAPEMIRIM",
      "ESTADO": "ES",
      "ENDERECO": "RUA 25 DE MARCO, 2",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CACHOEIRO DE ITAPEMIRIM",
      "ESTADO": "ES",
      "ENDERECO": "RUA CAPITAO DESLANDES, 81",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CALDAS NOVAS",
      "ESTADO": "GO",
      "ENDERECO": "RUA PEDRO BRANCO DE SOUSA S/N",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "CAMACARI",
      "ESTADO": "BA",
      "ENDERECO": "AV. COMER NO EDIFICIO CENTRO EMP NAW, 350",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMACARI",
      "ESTADO": "BA",
      "ENDERECO": "R. DUQUE DE CAXIAS,144",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMACARI",
      "ESTADO": "BA",
      "ENDERECO": "R. FRANCISCO DRUMOND,735",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMAQUA",
      "ESTADO": "RS",
      "ENDERECO": "RUA PRESIDENTE VARGAS, 607",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CAMBE",
      "ESTADO": "PR",
      "ENDERECO": "AV. INGLATERRA,753",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CAMETA",
      "ESTADO": "PA",
      "ENDERECO": "RUA CORONEL RAIMUNDO LEAO,778",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "CAMPINA GRANDE",
      "ESTADO": "PB",
      "ENDERECO": "R.MACIEL PINHEIRO,107",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMPINA GRANDE",
      "ESTADO": "PB",
      "ENDERECO": "R.VENANCIO NEIVA, 69",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMPINA GRANDE",
      "ESTADO": "PB",
      "ENDERECO": "R.CARDOSO VIEIRA , 107",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMPINA GRANDE",
      "ESTADO": "PB",
      "ENDERECO": "AV. PREFEITO SEVERINO BEZERRA CABRAL, 105",
      "BAIRRO": "CATOLE",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMPINA GRANDE",
      "ESTADO": "PB",
      "ENDERECO": "PC. DA BANDEIRA ,120",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMPINA GRANDE",
      "ESTADO": "PB",
      "ENDERECO": "AV. MARECHAL FLORIANO PEIXOTO, 812",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "ASSOCIACAO DOS PRODUTORES RURAIS DO SITIO SAO MIGUEL",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAMPO LARGO",
      "ESTADO": "PR",
      "ENDERECO": "RUA XV DE NOVEMBRO,2327",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CAMPO MAIOR",
      "ESTADO": "PI",
      "ENDERECO": "AV. DEMERVAL LOBAO,1241",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CANINDE",
      "ESTADO": "CE",
      "ENDERECO": "RUA ROMEU MARTINS,137",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CANOAS",
      "ESTADO": "RS",
      "ENDERECO": "RUA MUCK, 274",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CANOAS",
      "ESTADO": "RS",
      "ENDERECO": "AV. FARROUPILHA,4545",
      "BAIRRO": "MARECHAEL RANDON",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CAPANEMA",
      "ESTADO": "PA",
      "ENDERECO": "AV. BARAO DE CAPANEMA,1036",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "CARAPICUIBA",
      "ESTADO": "SP",
      "ENDERECO": "AV. RUI BARBOSA 262/264",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CARAPICUIBA",
      "ESTADO": "SP",
      "ENDERECO": "AV. RUI BARBOSA,909",
      "BAIRRO": "VILA SANTA TEREZINHA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CARAZINHO",
      "ESTADO": "RS",
      "ENDERECO": "AV. FLORES DA CUNHA,1678",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CARIACICA",
      "ESTADO": "ES",
      "ENDERECO": "AV. EXPEDITO GARCIA, 171",
      "BAIRRO": "CAMPO GRANDE",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CARIACICA",
      "ESTADO": "ES",
      "ENDERECO": "AV. EXPEDITO GARCIA , 92",
      "BAIRRO": "CAMPO GRANDE",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CARIACICA",
      "ESTADO": "ES",
      "ENDERECO": "ROD BR 262, 6555",
      "BAIRRO": "SAO FRANCISCO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CARIACICA",
      "ESTADO": "ES",
      "ENDERECO": "AV EXPEDITO GARCIA, 62",
      "BAIRRO": "CAMPO GRANDE",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CARPINA",
      "ESTADO": "PE",
      "ENDERECO": "AV. ESTACIO COIMBRA, 732",
      "BAIRRO": "SAO JOSE",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CARUARU",
      "ESTADO": "PE",
      "ENDERECO": "AV. RIO BRANCO, 188",
      "BAIRRO": "NOSSA SENHORA DAS DORES",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CASCAVEL",
      "ESTADO": "CE",
      "ENDERECO": "AV. PREFEITO VITORIANO ANTUNES,2484",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CASCAVEL",
      "ESTADO": "PR",
      "ENDERECO": "AV. BRASIL,6605",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CASCAVEL",
      "ESTADO": "PR",
      "ENDERECO": "RUA PADRE CHAMPAGNAT,119",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CASTANHAL",
      "ESTADO": "PA",
      "ENDERECO": "RUA MAXIMINO PORPINO,609",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "CATALAO",
      "ESTADO": "GO",
      "ENDERECO": "AV. 20 DE AGOSTO,1254",
      "BAIRRO": "SETOR CENTRAL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "CATALAO",
      "ESTADO": "GO",
      "ENDERECO": "AV 20 DE AGOSTO, 1793",
      "BAIRRO": "SETOR CENTRAL",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "CATANDUVA",
      "ESTADO": "SP",
      "ENDERECO": "RUA BRASIL,750",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CATOLE DO ROCHA",
      "ESTADO": "PB",
      "ENDERECO": "AV. DEPUTADO AMERICO MAIA , 45",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CAXIAS DO SUL",
      "ESTADO": "RS",
      "ENDERECO": "ROD RSC 453 QUILOMETRO 3,5 2780",
      "BAIRRO": "DESVIO RIZZO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CAXIAS DO SUL",
      "ESTADO": "RS",
      "ENDERECO": "RUA MOREIRA CESAR, 2785",
      "BAIRRO": "SAO PELEGRINO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CEARA-MIRIM",
      "ESTADO": "RN",
      "ENDERECO": "RUA GENERAL JOAO VARELA, 959",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "CERES",
      "ESTADO": "GO",
      "ENDERECO": "AV. BERNARDO SAYAO,319",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "CHAPECO",
      "ESTADO": "SC",
      "ENDERECO": "AV. GETULIO DORNELES VARGAS,115",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "LIONS",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CHAPECO",
      "ESTADO": "SC",
      "ENDERECO": "AV. SENADOR ATTILIO FONTANA,2583",
      "BAIRRO": "EFAPI",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "COLATINA",
      "ESTADO": "ES",
      "ENDERECO": "AV. GETULIO VARGAS,105",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "COLATINA",
      "ESTADO": "ES",
      "ENDERECO": "AV. GETULIO VARGAS,311",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CONCORDIA",
      "ESTADO": "SC",
      "ENDERECO": "RUA MARECHAL DEODORO, 989",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "COTIA",
      "ESTADO": "SP",
      "ENDERECO": "AV. PROF MANOEL JOSE PEDROSO,75",
      "BAIRRO": "PARQUE BAHIA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "CRICIUMA",
      "ESTADO": "SC",
      "ENDERECO": "AV. GETULIO VARGAS, 113",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CRICIUMA",
      "ESTADO": "SC",
      "ENDERECO": "RUA CORONEL PEDRO BENEDET,310",
      "BAIRRO": "Centro",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CRISTALINA",
      "ESTADO": "GO",
      "ENDERECO": "RUA GOIAS QUADRA,34 LOTE 13  57",
      "BAIRRO": "SETOR CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "CRUZEIRO DO SUL",
      "ESTADO": "AC",
      "ENDERECO": "AV. RODRIGUES ALVES, 362",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "CURITIBA",
      "ESTADO": "PR",
      "ENDERECO": "RUA IZAAC FERREIRA DA CRUZ, 2911",
      "BAIRRO": "SITIO CERCADO",
      "INSTITUICAO": "COLEGIO ESTADUAL HASDRUBAL BELLEGARD",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CURITIBA",
      "ESTADO": "PR",
      "ENDERECO": "AV. WINSTON CHURCHILL, 2630",
      "BAIRRO": "PINHEIRINHO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CURITIBA",
      "ESTADO": "PR",
      "ENDERECO": "ALD DR MURICY,600",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CURITIBA",
      "ESTADO": "PR",
      "ENDERECO": "RUA DESEMBARGADOR ERMELINO DE LEAO,35",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "CURRAIS NOVOS",
      "ESTADO": "RN",
      "ENDERECO": "R. ESCRIVAO ANTONIO QUINTINO,47",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "DIVINOPOLIS",
      "ESTADO": "MG",
      "ENDERECO": "AV. PRIMEIRO DE JUNHO, 871",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "DRACENA",
      "ESTADO": "SP",
      "ENDERECO": "AV. PRESIDENTE VARGA,452",
      "BAIRRO": "JD VERA CRUZ",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "DUQUE DE CAXIAS",
      "ESTADO": "RJ",
      "ENDERECO": "AV. NILO PECANHA , 140",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "DUQUE DE CAXIAS",
      "ESTADO": "RJ",
      "ENDERECO": "AV. PRESIDENTE VARGAS,187",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "DUQUE DE CAXIAS",
      "ESTADO": "RJ",
      "ENDERECO": "AV. GOVERNADOR LEONEL MOURA BRIZOLA,1640",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "DUQUE DE CAXIAS III",
      "ESTADO": "RJ",
      "ENDERECO": "RUA JOSE DE ALVARENGA 394",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ERECHIM",
      "ESTADO": "RS",
      "ENDERECO": "AV. MAURICIO CARDOSO,373",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ESPERANCA",
      "ESTADO": "PB",
      "ENDERECO": "RUA MANOEL RODRIGUES DE OLIVEIRA, 132",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "FARROUPILHA",
      "ESTADO": "RS",
      "ENDERECO": "RUA PINHEIRO MACHADO 20",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FERNANDOPOLIS",
      "ESTADO": "SP",
      "ENDERECO": "RUA BRASIL, 2133",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "FLORIANO",
      "ESTADO": "PI",
      "ENDERECO": "PRACA DR. SEBASTIAO MARTINS,304",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "FLORIANOPOLIS",
      "ESTADO": "SC",
      "ENDERECO": "R. JERONIMO COELHO,290",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FLORIANOPOLIS",
      "ESTADO": "SC",
      "ENDERECO": "RUA CONSELHEIRO MAFRA,98",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FLORIANOPOLIS",
      "ESTADO": "SC",
      "ENDERECO": "RUA FELIPE SCHMIDT,447",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FLORIANOPOLIS",
      "ESTADO": "SC",
      "ENDERECO": "RUA JERONIMO COELHO,170",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FLORIANOPOLIS",
      "ESTADO": "SC",
      "ENDERECO": "RUA JERONIMO COELHO,111",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FLORIANOPOLIS",
      "ESTADO": "SC",
      "ENDERECO": "AV PREFEITO OSMAR CUNHA , 472",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FORMOSA",
      "ESTADO": "GO",
      "ENDERECO": "R. VISCONDE DE PORTO SEGURO,619",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "FORTALEZA",
      "ESTADO": "CE",
      "ENDERECO": "RUA FLORIANO PEIXOTO, 621",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "LBV",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "FRANCA",
      "ESTADO": "SP",
      "ENDERECO": "R. MONSENHOR ROSA,1837",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "FRANCISCO BELTRAO",
      "ESTADO": "PR",
      "ENDERECO": "RUA PONTA GROSSA,1782",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "FREDERICO WESTPHALEN",
      "ESTADO": "RS",
      "ENDERECO": "RUA DO COMERCIO,454",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "GARANHUNS",
      "ESTADO": "PE",
      "ENDERECO": "RUA DR. JOSE MARIANO, 55",
      "BAIRRO": "SANTO ANTONIO CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GARANHUNS",
      "ESTADO": "PE",
      "ENDERECO": "AV. SANTO ANTONIO, 286",
      "BAIRRO": "SANTO ANTONIO  CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GARANHUNS",
      "ESTADO": "PE",
      "ENDERECO": "RUA DR. JOSE MARIANO, 165",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GOIANA",
      "ESTADO": "PE",
      "ENDERECO": "RUA BENJAMIN CONSTANTE,47",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GOIANESIA",
      "ESTADO": "GO",
      "ENDERECO": "AV. GOIAS, 374",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "GOIANIA",
      "ESTADO": "GO",
      "ENDERECO": "AV. CESAR LATTES,773",
      "BAIRRO": "SETOR NOVO HORIZONTE",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "GOIANINHA",
      "ESTADO": "RN",
      "ENDERECO": "RUA VIGARIO ANTONIO MONTENEGRO,125",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GRAVATA",
      "ESTADO": "PE",
      "ENDERECO": "RUA CLETO CAMPELO,81",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GRAVATAI",
      "ESTADO": "RS",
      "ENDERECO": "AV. DORVAL CANDIDO LUZ DE OLIVEIRA,83",
      "BAIRRO": "CENTRO/COHAB C",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "GUAIBA",
      "ESTADO": "RS",
      "ENDERECO": "RUA SAO JOSE,629",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "GUANAMBI",
      "ESTADO": "BA",
      "ENDERECO": "RUA RUI BARBOSA , 69",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GUARABIRA",
      "ESTADO": "PB",
      "ENDERECO": "AV. DOM PEDRO II, 422",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GUARABIRA",
      "ESTADO": "PB",
      "ENDERECO": "AV. OTACILIO LIRA CABRA , 1300",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "GUARUJA",
      "ESTADO": "SP",
      "ENDERECO": "AV. THIAGO FERREIRA,1048",
      "BAIRRO": "VILA ALICE VICENTE DE CARVALHO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "GUARULHOS",
      "ESTADO": "SP",
      "ENDERECO": "EST PRESIDENTE JUSCELINO KUBITSCHEK,5308",
      "BAIRRO": "JARDIM ALBERTINA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "GUARULHOS",
      "ESTADO": "SP",
      "ENDERECO": "RUA CAPITAO GABRIEL, 318",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "GUARULHOS",
      "ESTADO": "SP",
      "ENDERECO": "RUA JOAO GONCALVES,138",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "OLHAR DE BIA",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "IBIPORA",
      "ESTADO": "PR",
      "ENDERECO": "AV. SANTOS DUMONT,83",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ICARA",
      "ESTADO": "SC",
      "ENDERECO": "RUA CORONEL MARCOS ROVARIS, 259",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "IGUATU",
      "ESTADO": "CE",
      "ENDERECO": "RUA FLORIANO PEIXOTO,458",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "ROTARY CLUB",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "IJUI",
      "ESTADO": "RS",
      "ENDERECO": "RUA 14 DE JULHO,199",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "IRECE",
      "ESTADO": "BA",
      "ENDERECO": "AV. ADOLFO MOITINHO, 65",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "PROJETO NOVA CANAA",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ITABAIANA",
      "ESTADO": "PB",
      "ENDERECO": "AV. JOSE SILVEIRA, 35",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ITABERAI",
      "ESTADO": "GO",
      "ENDERECO": "PRACA  BALDUINO DA SILVA CALDAS S/N",
      "BAIRRO": "VILA LEONOR",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "ITABORAI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DR. PEREIRA DOS SANTOS,130",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITAGUAI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA PAULO DE FRONTIN,181",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITAGUAI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DR. CURVELO CAVALCANTI, 573",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITAJAI",
      "ESTADO": "SC",
      "ENDERECO": "RUA MANOEL VIEIRA GARCAO,30",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "ITAPERUNA",
      "ESTADO": "RJ",
      "ENDERECO": "AV. CARDOSO MOREIRA,870",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITAPETINGA",
      "ESTADO": "BA",
      "ENDERECO": "AL RUI BARBOSA, 6",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ITAPEVA",
      "ESTADO": "SP",
      "ENDERECO": "RUA MARIO PRANDINI, 376",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITAPIPOCA",
      "ESTADO": "CE",
      "ENDERECO": "AV ANASTACIO BRAGA, 632",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "ITAQUAQUECETUBA",
      "ESTADO": "SP",
      "ENDERECO": "RUA CAPITAO JOSE LEITE,49",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITAQUAQUECETUBA",
      "ESTADO": "SP",
      "ENDERECO": "AV. VER.  ALMIRO DIAS DE OLIVEIRA ,1112",
      "BAIRRO": "JARDIM NOVA ITAQUA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITUIUTABA",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―VINTE, 1025",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "ITUMBIARA",
      "ESTADO": "GO",
      "ENDERECO": "RUA PARANAIBA,422",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "AMI (ASSISTÊNCIA AO MENOR DE ITUMBIARA)",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "ITUPEVA",
      "ESTADO": "SP",
      "ENDERECO": "RUA RIO GRANDE DO SUL, 18",
      "BAIRRO": "VILA PARAIZO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JABOATAO DOS GUARARAPES",
      "ESTADO": "PE",
      "ENDERECO": "RUA SANTO ELIAS,29",
      "BAIRRO": "CAJUEIRO SECO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JABOATAO DOS GUARARAPES",
      "ESTADO": "PE",
      "ENDERECO": "AV. BARRETO DE MENEZES,800",
      "BAIRRO": "PIEDADE",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JABOTICABAL",
      "ESTADO": "SP",
      "ENDERECO": "RUA RUI BARBOSA, 808",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JACAREI",
      "ESTADO": "SP",
      "ENDERECO": "RUA OLIMPIO CATAO,500",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JACAREI",
      "ESTADO": "SP",
      "ENDERECO": "RUA DR. LUCIO MALTA,566",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JARAGUA DO SUL",
      "ESTADO": "SC",
      "ENDERECO": "AV. MARECHAL DEODORO DA FONSECA,406",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "JOAO CAMARA",
      "ESTADO": "RN",
      "ENDERECO": "RUA CICERO VARELA,178",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JOAO PESSOA",
      "ESTADO": "PB",
      "ENDERECO": "PRACA 1817 105 SHOPPING CIDADE",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JOAO PESSOA",
      "ESTADO": "PB",
      "ENDERECO": "RUA ANTONIO RABELO JUNIOR, 161",
      "BAIRRO": "MIRAMAR",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JOINVILLE",
      "ESTADO": "SC",
      "ENDERECO": "RUA IRIRIU, 997",
      "BAIRRO": "SAGUACU",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "JOINVILLE",
      "ESTADO": "SC",
      "ENDERECO": "RUA DO PRINCIPE, 292",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "JOINVILLE",
      "ESTADO": "SC",
      "ENDERECO": "RUA VISCONDE DE TAUNAY, 235",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "JOINVILLE",
      "ESTADO": "SC",
      "ENDERECO": "RUA IRIRIU,3584",
      "BAIRRO": "IRIRIU",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "JUAZEIRO",
      "ESTADO": "BA",
      "ENDERECO": "TRAVESSA BENJAMIN CONSTANT,186",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JUAZEIRO",
      "ESTADO": "BA",
      "ENDERECO": "RUA GOES CALMON, 22",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "JUIZ DE FORA",
      "ESTADO": "MG",
      "ENDERECO": "GALERIA PIO X 80",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JUIZ DE FORA",
      "ESTADO": "MG",
      "ENDERECO": "RUA SANTA RITA,458",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JUIZ DE FORA",
      "ESTADO": "MG",
      "ENDERECO": "AV. BARAO DO RIO BRANCO,2386",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JUNDIAI",
      "ESTADO": "SP",
      "ENDERECO": "RUA RANGEL PESTANA , 72",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "JUNDIAI",
      "ESTADO": "SP",
      "ENDERECO": "RUA DO ROSARIO, 539",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "LAGES",
      "ESTADO": "SC",
      "ENDERECO": "RUA MARECHAL DEODORO,191",
      "BAIRRO": "Copacabana",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LAGES",
      "ESTADO": "SC",
      "ENDERECO": "RUA CORONEL CORDOVA, 20",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LAGUNA",
      "ESTADO": "SC",
      "ENDERECO": "RUA SENADOR GUSTAVO RICHARD, 358",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LAJEADO",
      "ESTADO": "RS",
      "ENDERECO": "RUA JULIO DE CASTILHOS,1262",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LAURO DE FREITAS",
      "ESTADO": "BA",
      "ENDERECO": "RUA ROMUALDO DE BRITO,53",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "LIMOEIRO",
      "ESTADO": "PE",
      "ENDERECO": "RUA DA MATRIZ, 281",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "LINHARES",
      "ESTADO": "ES",
      "ENDERECO": "R. CAPITAO JOSE MARIA ,1341",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "R. PROFESSOR JOAO CANDIDO,100",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "R. SENADOR SOUZA NAVES,158",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "AV. BANDEIRANTES,666",
      "BAIRRO": "JARDIM IPIRANGA",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "AV. SAO PAULO,187",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "RUA SERGIPE,428",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "AV. SAUL ELKIND,1433",
      "BAIRRO": "AQUILLES STHENGEL",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "AV. AMERICO DEOLINDO GARLA, 224",
      "BAIRRO": "CENTRO/PACAEMBU",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LONDRINA",
      "ESTADO": "PR",
      "ENDERECO": "ROD CELSO GARCIA CID,5600",
      "BAIRRO": "GLEBA FAZENDA PALHANO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "LORENA",
      "ESTADO": "SP",
      "ENDERECO": "RUA DR. RODRIGUES DE AZEVEDO,12",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "LUZIANIA",
      "ESTADO": "GO",
      "ENDERECO": "PRACA EVANGELINO MEIRELES,122",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "MACAPA",
      "ESTADO": "AP",
      "ENDERECO": "RUA CANDIDO MENDES,1165",
      "BAIRRO": "CENTRAL",
      "INSTITUICAO": "CASA DA HOSPITALIDADE DE SANTANA",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "MACAPA",
      "ESTADO": "AP",
      "ENDERECO": "RUA LEOPOLDO MACHADO,2415",
      "BAIRRO": "CENTRAL",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "AV. COMENDADOR GUSTAVO PAIVA, 2990",
      "BAIRRO": "MANGABEIRAS",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "RUA DEPUTADO JOSE LAGES,431",
      "BAIRRO": "PONTA VERDE",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "RUA CINCINATO PINTO,180",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "AV. MENINO MARCELO, PATIO SHOPPING MACEIO, 3800",
      "BAIRRO": "CIDADE UNIVERSITARIA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "RUA DO COMERCIO, 327",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "LBV / OCULARE",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "RUA DO COMERCIO,463",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MACEIO",
      "ESTADO": "AL",
      "ENDERECO": "RUA BOA VISTA, 245",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MAGE",
      "ESTADO": "RJ",
      "ENDERECO": "RUA SANTA ELISA,137",
      "BAIRRO": "INHOMIRIM/ PIABETA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "MAMANGUAPE",
      "ESTADO": "PB",
      "ENDERECO": "R. MARCOS BARBOSA,362",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "MANAUS",
      "ESTADO": "AM",
      "ENDERECO": "AV. MARGARITA,1359",
      "BAIRRO": "NOVA CIDADE",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "MANAUS",
      "ESTADO": "AM",
      "ENDERECO": "AV. DJALMA BATISTA ,482",
      "BAIRRO": "PARQUE 10 DE NOVEMBRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "MANAUS",
      "ESTADO": "AM",
      "ENDERECO": "AV. COSME FERREIRA, 4605",
      "BAIRRO": "SAO JOSE OPERARIO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "MARINGA",
      "ESTADO": "PR",
      "ENDERECO": "AV. HERVAL,566",
      "BAIRRO": "ZONA 01",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "MARITUBA",
      "ESTADO": "PA",
      "ENDERECO": "TRAVESSA RAIMUNDO SANTANA,316",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "MOGI DAS CRUZES",
      "ESTADO": "SP",
      "ENDERECO": "RUA DR. PAULO FRONTIN , 261",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "MOGI DAS CRUZES",
      "ESTADO": "SP",
      "ENDERECO": "RUA DR. DEODATO WERTHEIMER,1680",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "MOGI DAS CRUZES",
      "ESTADO": "SP",
      "ENDERECO": "AV. VER. NARCISO YAGUE GUIMARAES, 1001",
      "BAIRRO": "CENTRO CIVICO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "MORRINHOS",
      "ESTADO": "GO",
      "ENDERECO": "AV. DR. GUMERCINDO OTERO S/N",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "MOSSORO",
      "ESTADO": "RN",
      "ENDERECO": "RUA SANTOS DUMONT,09",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV. RIO BRANCO, 564",
      "BAIRRO": "CIDADE ALTA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV. RIO BRANCO,557",
      "BAIRRO": "CIDADE ALTA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "RUA MOSSORO,602",
      "BAIRRO": "TIROL",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "RUA MOSSORO,598",
      "BAIRRO": "TIROL",
      "INSTITUICAO": "LBV",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV. CORONEL ESTEVAM,585",
      "BAIRRO": "ALECRIM",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "R.  AMARO BARRETO,1340",
      "BAIRRO": "ALECRIM",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV. BERNARDO VIEIRA,3775",
      "BAIRRO": "TIROL",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV. ENGENHEIRO ROBERTO FREIRE,340",
      "BAIRRO": "CAPIM MACIO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV SENADOR SALGADO FILHO, 2234",
      "BAIRRO": "CANDELARIA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NATAL",
      "ESTADO": "RN",
      "ENDERECO": "AV. DR. JOAO MEDEIROS FILHO, 2395",
      "BAIRRO": "POTENGI",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NILOPOLIS",
      "ESTADO": "RJ",
      "ENDERECO": "AV. GETULIO DE MOURA,1709",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NILOPOLIS",
      "ESTADO": "RJ",
      "ENDERECO": "ESTRADA  ANTONIO JOSE BITTENCOURT,170",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NITEROI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DA CONCEICAO,64",
      "BAIRRO": "Centro",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NITEROI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DA CONCEICAO,65",
      "BAIRRO": "Centro",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NITEROI",
      "ESTADO": "RJ",
      "ENDERECO": "AV. ERNANI DO AMARAL PEIXOTO,207",
      "BAIRRO": "Centro",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NITEROI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA NORONHA TORREZAO,02",
      "BAIRRO": "SANTA ROSA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NITEROI",
      "ESTADO": "RJ",
      "ENDERECO": "AV. SETE DE SETEMBRO, 201",
      "BAIRRO": "ICARAI",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NOVA CRUZ",
      "ESTADO": "RN",
      "ENDERECO": "RUA CORONEL JOSE DE BRITO,468",
      "BAIRRO": "SAO SEBASTIAO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "NOVA FRIBURGO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA FARINHA FILHO, 5",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NOVA IGUACU",
      "ESTADO": "RJ",
      "ENDERECO": "AV. MARECHAL FLORIANO PEIXOTO, 2215",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "NOVO HAMBURGO",
      "ESTADO": "RS",
      "ENDERECO": "RUA BENTO GONCALVES,2556",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "OSASCO",
      "ESTADO": "SP",
      "ENDERECO": "RUA DONA PRIMITIVA VIANCO,934",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "REDE SOCIAL DE OSASCO - QUE O BEM VIRE MODA",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "OSASCO",
      "ESTADO": "SP",
      "ENDERECO": "RUA DONA PRIMITIVA VIANCO,355",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "OSASCO",
      "ESTADO": "SP",
      "ENDERECO": "RUA  ANTONIO AGU,791",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "OSASCO",
      "ESTADO": "SP",
      "ENDERECO": "AV. DOS AUTONOMISTAS,1400",
      "BAIRRO": "VILA YARA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "OSASCO",
      "ESTADO": "SP",
      "ENDERECO": "RUA  ANTONIO AGU,681",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "PALHOCA",
      "ESTADO": "SC",
      "ENDERECO": "RUA JOSE MARIA DA LUZ,2827",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PALMARES",
      "ESTADO": "PE",
      "ENDERECO": "AV. LUIS DE FRANCA , 1370",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PALMEIRA DOS INDIOS",
      "ESTADO": "AL",
      "ENDERECO": "RUA MARIANO DE FREITAS, 5",
      "BAIRRO": "SAO CRISTOVAO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PARAGOMINAS",
      "ESTADO": "PA",
      "ENDERECO": "PRACA CELIO MIRANDA S/N",
      "BAIRRO": "CELIO MIRANDA MODELO I",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "PARNAIBA",
      "ESTADO": "PI",
      "ENDERECO": "AV. SAO SEBASTIAO,3429",
      "BAIRRO": "REIS VELOSO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PARNAIBA",
      "ESTADO": "PI",
      "ENDERECO": "RUA OSCAR CLARK, 612",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PARNAMIRIM",
      "ESTADO": "RN",
      "ENDERECO": "AV. BRIGADEIRO EVERALDO BREVES, 218",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PASSO FUNDO",
      "ESTADO": "RS",
      "ENDERECO": "RUA MOROM,1458",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PATO BRANCO",
      "ESTADO": "PR",
      "ENDERECO": "RUA GUARANI,465",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PATO BRANCO",
      "ESTADO": "PR",
      "ENDERECO": "RUA TAMOIO, 599",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PAU DOS FERROS",
      "ESTADO": "RN",
      "ENDERECO": "AV. DA INDEPENDENCIA, 890",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PAULISTA",
      "ESTADO": "PE",
      "ENDERECO": "RUA SIQUEIRA CAMPOS, 532",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "ALTINO VENTURA",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PELOTAS",
      "ESTADO": "RS",
      "ENDERECO": "RUA  ANDRADE NEVES,1750",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "COLEGIO MUNICIPAL PELOTENSE",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PELOTAS",
      "ESTADO": "RS",
      "ENDERECO": "RUA GENERAL NETO,1115",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PELOTAS",
      "ESTADO": "RS",
      "ENDERECO": "RUA MARECHAL DEODORO DA FONSECA , 702",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PENEDO",
      "ESTADO": "AL",
      "ENDERECO": "PC LARGO DE FATIMA, 20",
      "BAIRRO": "SANTA LUZIA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PESQUEIRA",
      "ESTADO": "PE",
      "ENDERECO": "PC GETULIO VARGAS,78",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PETROLINA",
      "ESTADO": "PE",
      "ENDERECO": "AV. SOUZA FILHO,461",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PETROLINA",
      "ESTADO": "PE",
      "ENDERECO": "AV. SOUZA FILHO,354",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "PETROPOLIS",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DO IMPERADOR, 419",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "PIRASSUNUNGA",
      "ESTADO": "SP",
      "ENDERECO": "RUA DUQUE DE CAXIAS,1501",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "PIRIPIRI",
      "ESTADO": "PI",
      "ENDERECO": "AV. SANTOS DUMOND,236",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "POCOS DE CALDAS",
      "ESTADO": "MG",
      "ENDERECO": "R.  ASSIS FIGUEIREDO,1315",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "POCOS DE CALDAS",
      "ESTADO": "MG",
      "ENDERECO": "AV. FRANCISCO SALLES,222",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "RIA MARECHAL FLORIANO PEIXOTO,165",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. BORGES DE MEDEIROS,312",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. ASSIS BRASIL,2127",
      "BAIRRO": "PASSO D AREIA",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. AZENHA,1194",
      "BAIRRO": "AZENHA",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "RUA GENERAL VITORINO,188",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. PRAIA DE BELAS,1181",
      "BAIRRO": "PRAIA DE BELAS",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. OTTO NIEMEYER,2727",
      "BAIRRO": "CAVALHADA",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. DIARIO DE NOTICIAS,300",
      "BAIRRO": "CRISTAL",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. ASSIS BRASIL,2611",
      "BAIRRO": "CRISTO REDENTOR",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO ALEGRE",
      "ESTADO": "RS",
      "ENDERECO": "AV. JOAO WALLIG,1800",
      "BAIRRO": "PASSO Dïŋ― AREIA",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "PORTO FERREIRA",
      "ESTADO": "SP",
      "ENDERECO": "RUA DONA BALBINA,609",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "POUSO ALEGRE",
      "ESTADO": "MG",
      "ENDERECO": "R. JOAO BASILIO,393",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "POUSO ALEGRE",
      "ESTADO": "MG",
      "ENDERECO": "RUA BERNARDINO DE CAMPOS,08",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "PRESIDENTE PRUDENTE",
      "ESTADO": "SP",
      "ENDERECO": "RUA BARAO DO RIO BRANCO,52",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "PRESIDENTE PRUDENTE",
      "ESTADO": "SP",
      "ENDERECO": "AV. SALIM FARAH MALUF,17",
      "BAIRRO": "JD DAS ROSAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "QUEIMADAS",
      "ESTADO": "PB",
      "ENDERECO": "RUA EUNICE RIBEIRO,434",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "QUEIMADOS",
      "ESTADO": "RJ",
      "ENDERECO": "AV. IRMAOS GUINLE,999",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "QUIXADA",
      "ESTADO": "CE",
      "ENDERECO": "RUA RODRIGUES JUNIOR,1273",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RECIFE",
      "ESTADO": "PE",
      "ENDERECO": "PC DA PAZ, 24",
      "BAIRRO": "AFOGADOS",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RECIFE",
      "ESTADO": "PE",
      "ENDERECO": "AV. BEBERIBE, 1923-A",
      "BAIRRO": "AGUA FRIA",
      "INSTITUICAO": "IGREJA NOSSA SENHORA DA PAZ",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RECIFE",
      "ESTADO": "PE",
      "ENDERECO": "RUA DR VICENTE MEIRA , 118",
      "BAIRRO": "ESPINHEIRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RECIFE",
      "ESTADO": "PE",
      "ENDERECO": "ESTRADA DO ENCANAMENTO,896",
      "BAIRRO": "CASA AMARELA",
      "INSTITUICAO": "FUNDACAO SANTA LUZIA",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RECIFE",
      "ESTADO": "PE",
      "ENDERECO": "AV. REPUBLICA DO LIBANO 251 SC 134",
      "BAIRRO": "PINA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RECIFE",
      "ESTADO": "PE",
      "ENDERECO": "RUA PADRE CARAPUCEIRO,777",
      "BAIRRO": "BOA VIAGEM",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "Recife",
      "ESTADO": "PE",
      "ENDERECO": "RUA FRANCISCO ALVES,173",
      "BAIRRO": "PAISSANDU",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RESENDE",
      "ESTADO": "RJ",
      "ENDERECO": "AV. ALBINO DE ALMEIDA , 138",
      "BAIRRO": "CAMPOS ELISEOS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RESENDE",
      "ESTADO": "RJ",
      "ENDERECO": "RUA ALFREDO WHATELY, 131",
      "BAIRRO": "CAMPOS ELISEOS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "R. SAO SEBASTIAO,745",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "R. VISCONDE DE INHAUMA,586",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "AV. DA SAUDADE,1044",
      "BAIRRO": "CAMPOS ELISEOS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "AV. DOM PEDRO I,1092",
      "BAIRRO": "IPIRANGA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "RUA SAO SEBASTIAO,564",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "AV. CORONEL FERNANDO FERREIRA LEITE,1540",
      "BAIRRO": "JD CALIFORNIA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIBEIRAO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "AV INDEPENDENCIA, 2476",
      "BAIRRO": "ALTO DA BOA VISTA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO BONITO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA VX DE NOVEMBRO,111",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO BRANCO",
      "ESTADO": "AC",
      "ENDERECO": "AV. BRASIL, 265",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "RIO BRANCO",
      "ESTADO": "AC",
      "ENDERECO": "AV. NACOES UNIDAS, 2941",
      "BAIRRO": "ESTACAO EXPERIMENTAL",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "RIO BRANCO",
      "ESTADO": "AC",
      "ENDERECO": "AV GETULIO VARGAS, 1624",
      "BAIRRO": "BOSQUE",
      "INSTITUICAO": "AMIGOS SOLIDARIOS",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "RIO DAS OSTRAS",
      "ESTADO": "RJ",
      "ENDERECO": "RODOVIA  AMARAL PEIXOTO,4525",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DAS OSTRAS",
      "ESTADO": "RJ",
      "ENDERECO": "AL. CARLOS LACERDA,159",
      "BAIRRO": "LIBERDADE",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA SANTO AFONSO, 230",
      "BAIRRO": "TIJUCA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. MARACANA,987",
      "BAIRRO": "TIJUCA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. SAO JOSEMARIA ESCRIVA, 560 - BLOCO 5 - APTO 305",
      "BAIRRO": "ITANHANGA",
      "INSTITUICAO": "PROJETO SEMEAR",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. GUIOMAR NOVAES S/N",
      "BAIRRO": "RECREIO DOS BANDEIRANTES",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. DAS AMERICAS,16401",
      "BAIRRO": "RECREIO DOS BANDEIRANTES",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "ESTRADA DO GALEAO,2775",
      "BAIRRO": "Portuguesa",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA MAREANTE,85",
      "BAIRRO": "Cocota",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. PASTOR MARTIN LUTHER KING JR,126",
      "BAIRRO": "DEL CASTILHO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "Rio de Janeiro",
      "ESTADO": "RJ",
      "ENDERECO": "AV. ENGENHEIRO SOUZA FILHO,1152",
      "BAIRRO": "RIO DAS PEDRAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA LOPES DE MOURA,15",
      "BAIRRO": "SANTA CRUZ",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA PROF CLEMENTE FERREIRA,1810",
      "BAIRRO": "BANGU",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. DE SANTA CRUZ,1380",
      "BAIRRO": "REALENGO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA FIGUEIREDO CAMARGO,281",
      "BAIRRO": "BANGU",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. RIO BRANCO,120",
      "BAIRRO": "CENTRO II",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA URUGUAIANA 11",
      "BAIRRO": "CENTRO II",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DO CATETE,64",
      "BAIRRO": "CATETE",
      "INSTITUICAO": "CIEP TANCREDO NEVES",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA DIAS DA CRUZ, 89",
      "BAIRRO": "MEIER",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA HADDOCK LOBO,373",
      "BAIRRO": "TIJUCA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "R DIAS DA CRUZ, 345",
      "BAIRRO": "MEIER",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA CARDOSO DE MORAIS, 237",
      "BAIRRO": "BONSUCESSO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. MERITI, 2198",
      "BAIRRO": "VILA DA PENHA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "TRAVESSA ALMERINDA FREITAS, 22",
      "BAIRRO": "MADUREIRA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA MARIA FREITAS,58",
      "BAIRRO": "MADUREIRA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. CESARIO DE MELO,3006",
      "BAIRRO": "CAMPO GRANDE",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "RUA CANDIDO BENICIO,1600",
      "BAIRRO": "PRACA SECA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. NELSON CARDOSO,1031",
      "BAIRRO": "TAQUARA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "AV. GEREMARIO DANTAS,1401",
      "BAIRRO": "FREGUESIA  JACAREPAGUA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DE JANEIRO",
      "ESTADO": "RJ",
      "ENDERECO": "ESTRADA DO TINDIBA,02381",
      "BAIRRO": "TAQUARA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "RIO DO SUL",
      "ESTADO": "SC",
      "ENDERECO": "RUA CARLOS GOMES,40",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "C E RICARDO MARCHI",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "RIO GRANDE",
      "ESTADO": "RS",
      "ENDERECO": "AV. SILVA PAES,350",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "RIO GRANDE",
      "ESTADO": "RS",
      "ENDERECO": "RUA ZALONY, 71",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "RIO LARGO",
      "ESTADO": "AL",
      "ENDERECO": "AV. PRES GETULIO VARGAS, 77",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "RUSSAS",
      "ESTADO": "CE",
      "ENDERECO": "RUA PADRE RAUL VIEIRA,633",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "CRIANCA CIDADA HOTARY CLUB",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SALGUEIRO",
      "ESTADO": "PE",
      "ENDERECO": "AV.  ANTONIO ANGELIM, 570",
      "BAIRRO": "SANTO ANTONIO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SALGUEIRO",
      "ESTADO": "PE",
      "ENDERECO": "AV. AGAMENON MAGALHAES, 565",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SALVADOR",
      "ESTADO": "BA",
      "ENDERECO": "RODOVIA BA-526 305",
      "BAIRRO": "SAO CRISTOVAO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SANTA CRUZ",
      "ESTADO": "RN",
      "ENDERECO": "PRACA PRESIDENTE GETULIO VARGAS,114",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SANTA CRUZ DO CAPIBARIBE",
      "ESTADO": "PE",
      "ENDERECO": "AV. PADRE ZUZINHA,224",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SANTA CRUZ DO SUL",
      "ESTADO": "RS",
      "ENDERECO": "RUA TENENTE CORONEL BRITO,947",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "SECRETARIA DE EDUCACAO DE SANTA CRUZ DO SUL. SECRETARIA DE EDUCACAO DE LAJEADO",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SANTA ISABEL DO PARA",
      "ESTADO": "PA",
      "ENDERECO": "AV. BENJAMIN CONSTANT,1037",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "SANTA MARIA",
      "ESTADO": "RS",
      "ENDERECO": "RUA DR. BOZANO,1073",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SANTA MARIA DE JETIBA",
      "ESTADO": "ES",
      "ENDERECO": "RUA HERMANN MIERTSCHINK,71",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SANTA RITA",
      "ESTADO": "PB",
      "ENDERECO": "R. SIQUEIRA CAMPOS,36",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SANTA ROSA",
      "ESTADO": "RS",
      "ENDERECO": "AV. AMERICA,478",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "ONG MAOS QUE SERVEM",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SANTO ANGELO",
      "ESTADO": "RS",
      "ENDERECO": "RUA MARECHAL FLORIANO,1652",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SANTOS",
      "ESTADO": "SP",
      "ENDERECO": "RUA FREI GASPAR, 80",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO CARLOS",
      "ESTADO": "SP",
      "ENDERECO": "AV. COMENDADOR ALFREDO MAFFEI,2269",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO CARLOS",
      "ESTADO": "SP",
      "ENDERECO": "AV. SAO CARLOS,1255",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO GONCALO",
      "ESTADO": "RJ",
      "ENDERECO": "TV ZEFERINO REIS, 119",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO GONCALO DO AMARANTE",
      "ESTADO": "RN",
      "ENDERECO": "AV. BEL TOMAZ LANDIM,359",
      "BAIRRO": "JARDIM LOLA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO JOAO DA BOA VISTA",
      "ESTADO": "SP",
      "ENDERECO": "RUA ADEMAR DE BARROS,132",
      "BAIRRO": "Centro",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOAO DE MERITI",
      "ESTADO": "RJ",
      "ENDERECO": "RUA EGAS MUNIZ,120",
      "BAIRRO": "VILAR DOS TELES",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE",
      "ESTADO": "SC",
      "ENDERECO": "AV. LEDIO JOAO MARTINS,649",
      "BAIRRO": "KOBRASOL",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SAO JOSE",
      "ESTADO": "SC",
      "ENDERECO": "ROD. SC 407 S/N",
      "BAIRRO": "PICADOS DO SUL",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SAO JOSE",
      "ESTADO": "SC",
      "ENDERECO": "AV. LEOBERTO LEAL,604",
      "BAIRRO": "BARREIROS",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SAO JOSE DE RIBAMAR",
      "ESTADO": "MA",
      "ENDERECO": "AV. GONCALVES DIAS,659",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO JOSE DE RIBAMAR",
      "ESTADO": "MA",
      "ENDERECO": "VIA COLETORA 2",
      "BAIRRO": "PARQUE VITORIA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO JOSE DE RIBAMAR",
      "ESTADO": "MA",
      "ENDERECO": "ESTRADA DE RIBAMAR, 166",
      "BAIRRO": "TIJUPA QUEIMADO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO JOSE DE RIBAMAR",
      "ESTADO": "MA",
      "ENDERECO": "ESTRADA DE RIBAMAR, 1000",
      "BAIRRO": "SITIO SARAMANTA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO JOSE DO RIO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "RUA MARECHAL DEODORO DA FONSECA,2943",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DO RIO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "AV. MIRASSOLANDIA , 1335",
      "BAIRRO": "CONJ HABITACIONAL COSTA DO SOL",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DO RIO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "RUA PRUDENTE DE MORAES, 2966",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DO RIO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "RUA BERNARDINO DE CAMPOS,3211",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DO RIO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "RUA VOLUNTARIOS DE SAO PAULO, 3877",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DO RIO PRETO",
      "ESTADO": "SP",
      "ENDERECO": "AV. PRESIDENTE JUSCELINO KUBITSCHEK , 5000",
      "BAIRRO": "IGUATEMI",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DOS CAMPOS",
      "ESTADO": "SP",
      "ENDERECO": "AV. DOUTOR ADHEMAR DE BARROS,1049",
      "BAIRRO": "JD SAO DIMAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO JOSE DOS CAMPOS",
      "ESTADO": "SP",
      "ENDERECO": "AV DOUTOR NELSON D AVILLA , 125",
      "BAIRRO": "JARDIM SAO DIMAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO LEOPOLDO",
      "ESTADO": "RS",
      "ENDERECO": "RUA INDEPENDENCIA,190",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "SAO LOURENCO DA MATA",
      "ESTADO": "PE",
      "ENDERECO": "AV. DOUTOR FRANCISCO CORREIA , 772",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. PRINCIPAL, 20",
      "BAIRRO": "SANTA EFIGENIA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. CORONEL COLARES MOREIRA,444",
      "BAIRRO": "JD RENASCENCA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA GRANDE,209",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. PROFESSOR CARLOS CUNHA,1000",
      "BAIRRO": "JARACATI",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. DANIEL DE LA TOUCHE,987",
      "BAIRRO": "COHAMA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA GRANDE,423",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. JERONIMO DE ALBUQUERQUE MARANHAO,20",
      "BAIRRO": "COHAB ANIL I",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA GRANDE,561",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA DO OUTEIRO,33A",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. GUAJAJARAS,30",
      "BAIRRO": "SAO CRISTOVAO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. JERONIMO DE ALBUQUERQUE MARANHAO,56",
      "BAIRRO": "COHAB ANIL III",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. PROFESSOR CARLOS CUNHA,1000",
      "BAIRRO": "JARACATI",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. CORONEL COLARES MOREIRA,444",
      "BAIRRO": "JD RENASCENCA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. SAO LUIS REI DE FRANCA,08",
      "BAIRRO": "TURU",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. DANIEL DE LA TOUCHE,987",
      "BAIRRO": "COHAMA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. PRINCIPAL, 20",
      "BAIRRO": "SANTA EFIGENIA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA DO OUTEIRO,33A",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. JERONIMO DE ALBUQUERQUE MARANHAO,20",
      "BAIRRO": "COHAB ANIL I",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA GRANDE,561",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. DOS HOLANDESES,03A",
      "BAIRRO": "CALHAU",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. GUAJAJARAS,30",
      "BAIRRO": "SAO CRISTOVAO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. JERONIMO DE ALBUQUERQUE MARANHAO,56",
      "BAIRRO": "COHAB ANIL III",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA GRANDE,209",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. JOAO PESSOA,188",
      "BAIRRO": "JOAO PAULO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "AV. EUCLIDES FIGUEIREDO,1000",
      "BAIRRO": "JARACATY",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO LUIS",
      "ESTADO": "MA",
      "ENDERECO": "RUA GRANDE,423",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAO MATEUS",
      "ESTADO": "ES",
      "ENDERECO": "RUA DR.  ARLINDO SODRE,490",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA SAO BENTO,185",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "IGREJA SAO FRANCISCO",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA VOLUNTARIOS DA PATRIA,2292",
      "BAIRRO": "SANTANA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA  AUGUSTA,2449",
      "BAIRRO": "CERQUEIRA CESAR",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA TREZE DE MAIO,1870",
      "BAIRRO": "BELA VISTA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA PEDREIRA DO ROQUE S/N",
      "BAIRRO": "VILA CORBERI",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA DOZE OUTUBRO,466",
      "BAIRRO": "LAPA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA GREENFELD,252",
      "BAIRRO": "IPIRANGA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA PADRE ANTONIO BENEDITO,46",
      "BAIRRO": "PENHA DE FRANCA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA TIBURCIO DE SOUSA, 71",
      "BAIRRO": "ITAIM PAULISTA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA CARDOSO DE ALMEIDA , 617",
      "BAIRRO": "PERDIZES",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "RUA DA MOOCA,2202",
      "BAIRRO": "MOOCA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "R. TEODORO SAMPAIO,2124",
      "BAIRRO": "PINHEIROS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "R TAVANNES, 389",
      "BAIRRO": "LAUZANE PAULISTA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO PAULO",
      "ESTADO": "SP",
      "ENDERECO": "AV PARADA PINTO, 338",
      "BAIRRO": "VILA NOVA CACHOEIRINHA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SAO RAIMUNDO NONATO",
      "ESTADO": "PI",
      "ENDERECO": "TRAV. TENENTE ZECA RUBENS,60",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SAPE",
      "ESTADO": "PB",
      "ENDERECO": "AV. COMEND RENATO RIBEIRO COUTINHO,1261",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SERRA",
      "ESTADO": "ES",
      "ENDERECO": "AV. CENTRAL, 921",
      "BAIRRO": "LARANJEIRAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SERRA",
      "ESTADO": "ES",
      "ENDERECO": "AV. JOAO PALACIOS, 300",
      "BAIRRO": "EURICO SALES",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SERRA",
      "ESTADO": "ES",
      "ENDERECO": "AV. CENTRAL, 870",
      "BAIRRO": "PQ RES LARANJEIRAS",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SERRA TALHADA",
      "ESTADO": "PE",
      "ENDERECO": "RUA MANOEL PEREIRA DA SILVA, 745",
      "BAIRRO": "NOSSA SENHORA DA PENHA",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SERRINHA",
      "ESTADO": "BA",
      "ENDERECO": "PRACA LUIZ NOGUEIRA, 415",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SERTAOZINHO",
      "ESTADO": "SP",
      "ENDERECO": "R. BARAO DO RIO BRANCO,1153",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SOBRAL",
      "ESTADO": "CE",
      "ENDERECO": "RUA CORONEL JOSE SABOIA,593",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SOLANEA",
      "ESTADO": "PB",
      "ENDERECO": "RUA CELSO CIRNE,536",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SOROCABA",
      "ESTADO": "SP",
      "ENDERECO": "RUA QUINZE DE NOVEMBRO,247",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SOROCABA",
      "ESTADO": "SP",
      "ENDERECO": "RUA DOUTOR BRAGUINHA,226",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "COMAS (VOTORANTIM)",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "SORRISO",
      "ESTADO": "MT",
      "ENDERECO": "AV. NATALINO JOAO BRESCANSIN, 1763",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "SURUBIM",
      "ESTADO": "PE",
      "ENDERECO": "R. 15 DE NOVEMBRO,44",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "JUNTAMENTE COM A PRIMEIRA DAMA DA CIDADE, ONGS DIRECIONADO PELA PREFEITURA",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "SUZANO",
      "ESTADO": "SP",
      "ENDERECO": "RUA SETE DE SETEMBRO,555",
      "BAIRRO": "PARQUE SUZANO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "TAILANDIA",
      "ESTADO": "PA",
      "ENDERECO": "TV. BREVES ESQ COM AV NATAL,42",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "TEIXEIRA DE FREITAS",
      "ESTADO": "BA",
      "ENDERECO": "AV. MAL CASTELO BRANCO,760",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "R. BARROSO,268",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "R. RUI BARBOSA, 154",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "R. 13 DE MAIO,240",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "RUA SENADOR TEODORO PACHECO,1102",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "R. COELHO RODRIGUES,1310",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "RUA COELHO DE RESENDE,162",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "AV. JOSE FRANCISCO DE ALMEIDA NETO,10",
      "BAIRRO": "ITARARE",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "R. PIRES DE CASTRO,632",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "AV. ININGA ,1201",
      "BAIRRO": "JOQUEI CLUBE",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "R. SENADOR CANDIDO FERRAZ,1250",
      "BAIRRO": "JOCKEY",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "AV. RAUL LOPES,1000",
      "BAIRRO": "NOIVOS",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "RUA FELIX PACHECO,2100",
      "BAIRRO": "CENTRO SUL",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "AV. MARECHAL JUAREZ TAVORA ,18",
      "BAIRRO": "PARQUE PIAUI",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "AV. MARECHAL CASTELO BRANCO,911",
      "BAIRRO": "CABRAL",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TERESINA",
      "ESTADO": "PI",
      "ENDERECO": "RUA BARROSO, 260",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TIMBAUBA",
      "ESTADO": "PE",
      "ENDERECO": "RUA DOUTOR ALCEBIADES,319",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TIMON",
      "ESTADO": "MA",
      "ENDERECO": "AV. PRESIDENTE MEDICI,439",
      "BAIRRO": "PARQUE PIAUI",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "TRES CORACOES",
      "ESTADO": "MG",
      "ENDERECO": "RUA LUCIANO PEREIRA PENHA,156",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "TRES RIOS",
      "ESTADO": "RJ",
      "ENDERECO": "RUA PREFEITO WALTER FRANCKLIN,42",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "TRINDADE",
      "ESTADO": "GO",
      "ENDERECO": "RUA CORONEL ANACLETO,350",
      "BAIRRO": "VILA JARDIM SALVADOR",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "TUBARAO",
      "ESTADO": "SC",
      "ENDERECO": "AV. MARCOLINO MARTINS CABRAL, 1001",
      "BAIRRO": "CENTRO TUB",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "TUCURUI",
      "ESTADO": "PA",
      "ENDERECO": "RUA TRINTA E UM DE MARCO,318",
      "BAIRRO": "SANTA ISABEL",
      "INSTITUICAO": "",
      "REGIAO": "NORTE"
    },
    {
      "CIDADE": "UBERLANDIA",
      "ESTADO": "MG",
      "ENDERECO": "RUAïŋ―OLEGARIO MACIEL, 469",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "UBERLANDIA",
      "ESTADO": "MG",
      "ENDERECO": "AV. AFONSO PENA,55",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "UNAI",
      "ESTADO": "MG",
      "ENDERECO": "AV. GOVERNADOR VALADARES,1200",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "UNIAO DOS PALMARES",
      "ESTADO": "AL",
      "ENDERECO": "RUA HERMANO PLECH, 261",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "URUGUAIANA",
      "ESTADO": "RS",
      "ENDERECO": "RUA DOMINGOS DE ALMEIDA,1841",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "VALPARAISO DE GOIAS",
      "ESTADO": "GO",
      "ENDERECO": "QUADRA 06 S/N",
      "BAIRRO": "VALPARAISO I",
      "INSTITUICAO": "",
      "REGIAO": "CENTROOESTE"
    },
    {
      "CIDADE": "VARGINHA",
      "ESTADO": "MG",
      "ENDERECO": "R. DOUTOR WENCESLAU BRAZ, 262",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VIAMAO",
      "ESTADO": "RS",
      "ENDERECO": "RUA REVERENDO AMERICO VESPUCIO CABRAL,322",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    },
    {
      "CIDADE": "VILA VELHA",
      "ESTADO": "ES",
      "ENDERECO": "AV. JERONIMO MONTEIRO, 264",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VILA VELHA",
      "ESTADO": "ES",
      "ENDERECO": "RUA LUCIANO DAS NEVES, 2418",
      "BAIRRO": "DIVINO ESPIRITO SANTO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VILA VELHA",
      "ESTADO": "ES",
      "ENDERECO": "AV. JERONIMO MONTEIRO, 1395",
      "BAIRRO": "CENTRO DE VILA VELHA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VILA VELHA",
      "ESTADO": "ES",
      "ENDERECO": "AV. DOUTOR OLIVIO LIRA, 353",
      "BAIRRO": "PRAIA DA COSTA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VILA VELHA",
      "ESTADO": "ES",
      "ENDERECO": "ROD DO SOL 5000",
      "BAIRRO": "JOCKEY DE ITAPARICA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VITORIA",
      "ESTADO": "ES",
      "ENDERECO": "AV. JERONIMO MONTEIRO, 690",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VITORIA",
      "ESTADO": "ES",
      "ENDERECO": "AV. AMERICO BUAIZ , 200",
      "BAIRRO": "ENSEADA DO SUA",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VITORIA",
      "ESTADO": "ES",
      "ENDERECO": "AV. NOSSA SENHORA DA PENHA , 570",
      "BAIRRO": "PRAIA DO CANTO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VITORIA DA CONQUISTA",
      "ESTADO": "BA",
      "ENDERECO": "RUA FRANCISCO SANTOS, 149",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "VITORIA DE SANTO ANTAO",
      "ESTADO": "PE",
      "ENDERECO": "RUA DR ALUISIO DE MELO XAVIER,43",
      "BAIRRO": "MATRIZ",
      "INSTITUICAO": "",
      "REGIAO": "NORDESTE"
    },
    {
      "CIDADE": "VOTORANTIM",
      "ESTADO": "SP",
      "ENDERECO": "AV. 31 DE MARCO,515",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "VOTUPORANGA",
      "ESTADO": "SP",
      "ENDERECO": "RUA AMAZONAS,3479",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUDESTE"
    },
    {
      "CIDADE": "XANXERE",
      "ESTADO": "SC",
      "ENDERECO": "RUA CORONEL PASSOS MAIA,726",
      "BAIRRO": "CENTRO",
      "INSTITUICAO": "",
      "REGIAO": "SUL"
    }
  ]