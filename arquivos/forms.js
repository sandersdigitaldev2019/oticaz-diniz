$(document).ready(function() {
	var header = {
	    'Accept': 'application/json',
	    'REST-range': 'resources=0-10',
	    'Content-Type': 'application/json; charset=utf-8'
	};
	
	var insertMasterData = function (ENT, loja, dados, fn) {
		$.ajax({
		    url: '/api/dataentities/' + ENT + '/documents/',
		    type: 'PATCH',
		    data: dados,
			headers:header,
		    success: function (res) {
		        fn(res);
		    },
		    error: function (res) {
				swal("Oops", "Houve um erro tente mais tarde!", "error");
			}
		});
	};

	$('#formContatoLoja').on('submit', function (event) {
		event.preventDefault();
		$('#formContatoLoja #btnEnviar').addClass('disable');

		var nome = $('#formContatoLoja #nome').val();
		var email = $('#formContatoLoja #email').val();
		var telefone = $('#formContatoLoja #telefone').val();
		var mensagem = $('#formContatoLoja #mensagem').val();

		var obj_dados = {
			"nome": nome,
			"email": email,
			"telefone": telefone,
			"mensagem": mensagem
		}

		var json_dados = JSON.stringify(obj_dados);
		
		console.log(json_dados)

		insertMasterData("AL", 'oticasdiniz', json_dados, function (res) {
			console.log(res);
			swal("Obrigado pela mensagem!", "Recebemos sua mensagem com sucesso, aguarde nosso contato", "success");
			$('#formContatoLoja input, #formContatoLoja textarea').val('');
			$('#formContatoLoja #btnEnviar').removeClass('disable');
		});
	});
	
	$('#formContatoLojaFisica').on('submit', function (event) {
		event.preventDefault();
		$('#formContatoLojaFisica #btnEnviar').addClass('disable');

		var nome = $('#formContatoLojaFisica #nome').val();
		var email = $('#formContatoLojaFisica #email').val();
		var telefone = $('#formContatoLojaFisica #telefone').val();
		var endereco = $('#formContatoLojaFisica #endereco').val();
		var cidade = $('#formContatoLojaFisica #cidade').val();
		var mensagem = $('#formContatoLojaFisica #mensagem').val();

		var obj_dados = {
			"nome": nome,
			"email": email,
			"telefone": telefone,
			"endereco": endereco, 
			"cidade": cidade, 
			"mensagem": mensagem
		}

		var json_dados = JSON.stringify(obj_dados);
		
		console.log(json_dados)

		insertMasterData("AF", 'oticasdiniz', json_dados, function (res) {
			console.log(res);
			swal("Obrigado pela mensagem!", "Recebemos sua mensagem com sucesso, aguarde nosso contato", "success");
			$('#formContatoLojaFisica input, #formContatoLojaFisica textarea').val('');
			$('#formContatoLojaFisica #btnEnviar').removeClass('disable');
		});
	});
	
	$('#formSejaUmFranqueado').on('submit', function (event) {
		event.preventDefault();
		$('#formSejaUmFranqueado .enviar').addClass('disable');

		var nome = $('#formSejaUmFranqueado #nome').val();
		var email = $('#formSejaUmFranqueado #email').val();
		var telefone_ddd = $('#formSejaUmFranqueado #telefone_ddd').val();
		var telefone = $('#formSejaUmFranqueado #telefone').val();
		var celular_ddd = $('#formSejaUmFranqueado #celular_ddd').val();
		var celular = $('#formSejaUmFranqueado #celular').val();
		var endereco = $('#formSejaUmFranqueado #endereco').val();
		var cidade = $('#formSejaUmFranqueado #cidade').val();
		var estado = $('#formSejaUmFranqueado #estadoselect option:selected').val();
		var estado_pretendido = $('#formSejaUmFranqueado select#estado_pretendido option:selected').val();
		var cidade_pretendida = $('#formSejaUmFranqueado #cidade_pretendida').val();
		var atua_ramo = $('#formSejaUmFranqueado select#atua_ramo option:selected').val();
		var possui_ponto = $('#formSejaUmFranqueado select#possui_ponto option:selected').val();
		var estado_ponto = $('#formSejaUmFranqueado select#estado_ponto option:selected').val();
		var cidade_ponto = $('#formSejaUmFranqueado #cidade_ponto').val();
		var endereco_ponto = $('#formSejaUmFranqueado #endereco_ponto').val();
		var mensagem = $('#formSejaUmFranqueado #comentario').val();

		var obj_dados = {
			'nome': nome,
			'email': email,
			'telefone_ddd': telefone_ddd,
			'telefone': telefone,
			'celular_ddd': celular_ddd,
			'celular': celular,
			'endereco': endereco,
			'cidade': cidade,
			'estado': estado,
			'estado_pretendido': estado_pretendido,
			'cidade_pretendida': cidade_pretendida,
			'atua_ramo': atua_ramo,
			'possui_ponto': possui_ponto,
			'estado_ponto': estado_ponto,
			'cidade_ponto': cidade_ponto,
			'endereco_ponto': endereco_ponto,
			'comentario': mensagem
		}

		var json_dados = JSON.stringify(obj_dados);
		
		console.log(json_dados)

		insertMasterData("SF", 'oticasdiniz', json_dados, function (res) {
			console.log(res);
			swal("Obrigado pela mensagem!", "Recebemos sua mensagem com sucesso, aguarde nosso contato", "success");
			$('#formSejaUmFranqueado input, #formSejaUmFranqueado textarea').val('');
			$('#formSejaUmFranqueado .enviar').removeClass('disable');
		});
	});
});